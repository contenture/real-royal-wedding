module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'indent': ['error', 2, {
      SwitchCase: 2,
      VariableDeclarator: 2,
      outerIIFEBody: 2,
      FunctionDeclaration: {
        parameters: 2,
        body: 2
      },
      CallExpression: {
        arguments: 2
      },
      ArrayExpression: 2,
      ObjectExpression: 2,
      ImportDeclaration: 2,
      flatTernaryExpressions: false,
      ignoreComments: false
    }]
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
