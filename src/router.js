import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import SaveTheDate from './views/SaveTheDate';
import MainNavbar from './views/MainNavbar';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      components: {
        default: Home,
        header: MainNavbar
      },
      props: {
        header: { colorOnScroll: 10 }
      }
    },
    {
      path: '/save-the-date',
      name: 'save-the-date',
      component: SaveTheDate
    }
  ],
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }

    const position = {};

    if (to.hash) {
      position.selector = to.hash;
    }

    return document.querySelector(position.selector).scrollIntoView({behavior: 'smooth'});
  }
});
